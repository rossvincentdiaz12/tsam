@extends('partial.usertemplate') 

@section ('title','Tsam- Report Asset')

@section ('navtitle','Tsam Report Assets')
 

@section('items')

<h3 style="color: blue;">Tickets</h3></br>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id:</th>
				<th>Comment:</th>
                <th>Date:</th>
                <th>Concern:</th>
                <th>User Id:</th>
                <th>Borrow Id:</th>
                <th>Status:</th>
				<th>Action:</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($ticket as $ticket)
				<tr>
					<th>{{$ticket->id}}</th>
					<th>{{$ticket->comment}}</th>
                    <th>{{$ticket->date}}</th>
                    <th>{{$ticket->concern->name}}</th>
                    <th>{{$ticket->user->name}}</th>
                    <th>{{$ticket->borrow_id}}</th>
                    <th>{{$ticket->status->name}}</th>
					<th><form action="/ticketcancel/{{$ticket->id}}" method="POST">
							@csrf
							@method('DELETE')		
							<button class="btn btn-danger" type="submit">Cancel</button>
					</form></th>
					
						<th><a href="/editticket/{{$ticket->id}}" class="btn btn-primary">Edit</a></th>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

{{-- <script type="text/javascript">

	const borrowtable = (id)=>{
		console.log(id)
		// let quantity = document.querySelector("#quantity_"+id).value;
		// alert(quantity + " of item " + itemName + " has been added to cart");

		let data = new FormData;

		data.append("_token", "{{ csrf_token() }}");
		data.append("_method", "Delete");
		// data.append("quantity", quantity);

		fetch("/borrowtable/"+id,{
			method: "post",
			body: data
		}).then(res=>res.text())
		.then(res=>console.log(res))
	}

</script> --}}



@endsection