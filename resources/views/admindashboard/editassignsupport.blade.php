@extends('partial.template') 

@section ('title','Tsam- Add Asset')

@section ('navtitle','Tsam Add Assets')

@section('button')

<div class="col-lg-6 offset-lg-3">
	<form action="/editassignsupport/{{$ticketsupport->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
        <div class="form-group">
			<label for="support_id">Support:</label>
			<select name="support_id" class="form-control">
				@foreach($support as $support)
				<option value="{{$support->id}}" {{$support->id == $ticketsupport->id ? "selected" : ""}}>{{$support->name}}</option>
				@endforeach
			</select>
		</div>

        <div class="form-group">
			<label for="ticket_id">Ticket:</label>
			<select name="ticket_id" class="form-control">
				@foreach($ticket as $ticket)
				<option value="{{$ticket->id}}">{{$ticket->id}}</option>
				@endforeach
			</select>
		</div>
	
		<button class="btn btn-primary" type="submit">Edit tickets</button>
	</form>
</div>


@endsection

