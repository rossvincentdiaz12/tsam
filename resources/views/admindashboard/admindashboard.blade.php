@extends('partial.template') 

@section ('title','Tsam')

@section ('navtitle','Tsam Dashboard')

@section('button')

  
<div class="assign-suppory">
    <a href="../assignsupport" class="btn btn-sq-lg">
        <i class="fa fa-user fa-5x"></i>
        <p>Assign Support </p>
    </a>
</div>

<div class="done-ticket">
    <a href="#e" class="btn btn-sq-lg">
        <i class="fa fa-ticket-alt fa-5x"></i>
        <p>Done Ticket </p>
    </a>
</div>

<div class="add-asset">
    <a href="#e" class="btn btn-sq-lg">
        <i class="fas fa-shopping-cart fa-5x"></i>
        <p>Add Asset</p>
    </a>
</div>
            

@endsection


@section('table')


<h3 style="color: blue;">All Users</h3></br>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name:</th>
				<th>Email Address:</th>
				<th>Action:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<th>{{$user->name}}</th>
					<th>{{$user->email}}</th>
					<th></th>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>


@endsection

@section('map')

        <div id="mapdiv">
			<h3 style="color:#4360b5;">Active Borrow Asset</h3></br>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3872.3252730730555!2d121.61229391422093!3d13.939227996679543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33bd4b2067db18f5%3A0x73ec96852e5b1bd4!2sBrother&#39;s%20Seafood%20%26%20Inuman!5e0!3m2!1sen!2sph!4v1579830247086!5m2!1sen!2sph" width=100% height="1000" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        
@endsection