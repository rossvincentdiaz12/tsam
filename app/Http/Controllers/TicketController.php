<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Concern;

use \App\Borrow;

use \App\Type;

use \App\Status;

use \App\Ticket;

use \App\Asset;
use \App\Support;

use \App\User;
use \App\TicketSupport;


use Auth;


class TicketController extends Controller
{
    //

    public function viewticket(){

        
        return view('userdashboard.ticket');
    }

    public function createReportTicket($id){
        $borrow = Borrow::find($id);
        $concerns = Concern::all();
        $types = Type::all();
        $status = Status::all();

        $ticket = Ticket::all();

        return view ('userdashboard.ticket', compact('borrow', 'types','status','ticket','concerns'));
    }
    
    public function addticket(Request $req){
    	$user = Auth::user();
    	// dd($req);

    	$newticket = new Ticket;
    	$newticket->comment = $req->comment;
    	$newticket->concern_id = $req->Concernid;
        $newticket->user_id = $req->userid;
        $newticket->borrow_id = $req->borrowid;
        $newticket->status_id = $req->statusid;
    	$newticket->save();

    	return redirect()->back();

    }

    public function editticket($id){
        $ticket = Ticket::find($id);
        $concerns = Concern::all();
        $status = Status::all();
        $user = User::all();

        return view ('admindashboard.editticket', compact('ticket', 'status','concerns','user'));
    }
    
    public function update($id, Request $req){
        //validate
    
        // dd($req);

        $ticket = Ticket::find($id);
        $ticket->comment = $req->comment;
        $ticket->concern_id = $req->concern_id;




        $ticket->save();

     

        return redirect('/assignsupport');

    }
    

    public function viewtickeruser(){
        $ticketsupport = TicketSupport::all();

        $ticket = Ticket::all();

        $tickets = Ticket::all(); 

        $support = Support::all();

        $concerns = Concern::all();
        $user = User::all();
        $status = Status::all();



        return view('userdashboard.viewticket', compact('ticketsupport','ticket','support','tickets','concerns','user','status','support'));
        
    }
}
