<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    //

    public function users(){
    	return $this->belongsToMany('\App\User');
    }
    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function asset(){
    	return $this->belongsTo('\App\Asset');
    }
    public function status(){
    	return $this->belongsTo('\App\Status');
    }
    public function type(){
    	return $this->belongsTo('\App\Type');
    }

    public function ticket(){
        return $this->belongsTo('\App\Ticket');
    }
}
