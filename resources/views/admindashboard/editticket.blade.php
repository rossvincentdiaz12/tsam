@extends('partial.template') 

@section ('title','Tsam- Add Asset')

@section ('navtitle','Tsam Add Assets')

@section('button')

<div class="col-lg-6 offset-lg-3">
	<form action="/editticket/{{$ticket->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="comment">Comment:</label>
			<input type="text" name="comment" class="form-control" value="{{$ticket->comment}}">
		</div>

        <div class="form-group">
			<label for="concern_id">Type:</label>
			<select name="concern_id" class="form-control">
				@foreach($concerns as $concerns)
				<option value="{{$concerns->id}}" {{$concerns->id == $ticket->id ? "selected" : ""}}>{{$concerns->name}}</option>
				@endforeach
			</select>
		</div>
	
		<button class="btn btn-primary" type="submit">Edit tickets</button>
	</form>
</div>


@endsection

